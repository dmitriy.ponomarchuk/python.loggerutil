from LoggerUtil import LoggerUtil

logger = LoggerUtil(__name__).getLogger()

logger.info('Info')
logger.debug('Debug')
logger.error('Error')
logger.warning('Warning')
