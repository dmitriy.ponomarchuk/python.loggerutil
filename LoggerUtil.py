import logging
from datetime import datetime

class LoggerUtil:
    def __init__(self, logger_name: str):
        self.FILE_NAME = 'log/' + logger_name + '.' + datetime.now().strftime('%Y-%m-%d') + '.log'
        self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        # create logger
        self.logger = logging.getLogger(logger_name)
        self.logger.setLevel(logging.DEBUG)

        # create console handler and set level to debug
        self.ch = logging.StreamHandler()
        self.fh = logging.FileHandler(self.FILE_NAME)

        # create formatter and add it to the handlers
        self.fh.setFormatter(self.formatter)
        self.ch.setFormatter(self.formatter)

        # add the handlers to the logger
        self.logger.addHandler(self.fh)
        self.logger.addHandler(self.ch)

    def getLogger(self) -> 'logger':
        return self.logger